package com.itb.geoguesser;

public class QuestionModel {
    int questionImage;
    int answer;

    public QuestionModel(int questionImage, int answer) {
        this.questionImage = questionImage;
        this.answer = answer;
    }

    public int getQuestionImage() {
        return questionImage;
    }

    public void setQuestionImage(int questionImage) {
        this.questionImage = questionImage;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

}
