package com.itb.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    TextView progressText;
    ImageView imageView;
    Button aButton;
    Button bButton;
    Button cButton;
    Button dButton;
    Button hint_button;


    int currentQuestionId = 0, numberHint = 3;
    double score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressText = findViewById(R.id.progress_text);
        imageView = findViewById(R.id.question_image);
        aButton = findViewById(R.id.a_button);
        bButton = findViewById(R.id.b_button);
        cButton = findViewById(R.id.c_button);
        dButton = findViewById(R.id.d_button);
        hint_button = findViewById(R.id.hint_button);



        aButton.setOnClickListener(this);
        bButton.setOnClickListener(this);
        cButton.setOnClickListener(this);
        dButton.setOnClickListener(this);
        hint_button.setOnClickListener(this);


        shaffleQuestion();
        refreshScreen();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.a_button:
                if (!(currentQuestionId >= 9)){
                    checkAnswer(1);
                    currentQuestionId++;
                }else isFinish();
                break;
            case R.id.b_button:
                if (!(currentQuestionId >= 9)){
                    checkAnswer(2);
                    currentQuestionId++;
                }else isFinish();
                break;
            case R.id.c_button:
                if (!(currentQuestionId >= 9)){
                    checkAnswer(3);
                    currentQuestionId++;
                }else isFinish();
                break;
            case R.id.d_button:
                if (!(currentQuestionId >= 9)){
                    checkAnswer(4);
                    currentQuestionId++;
                }else isFinish();
                break;
            case R.id.hint_button:
                showHint(1);
                showHint(2);
                showHint(3);
                showHint(4);
        }
        refreshScreen();
    }

    public void showHint(int answer){
        int AnswerIsCorrect = GeoGuesserViewModel.questionBank[currentQuestionId].getAnswer();
        if (numberHint > 0){
            if (AnswerIsCorrect == answer){
                Toast.makeText(this, "The correct answer is " + AnswerIsCorrect, Toast.LENGTH_SHORT).show();
                numberHint--;
                score = score - 1;
            }
        }
        if (numberHint == 0){
            hint_button.setVisibility(View.INVISIBLE);
        }
    }



    public void checkAnswer (int answer){
        int AnswerIsCorrect = GeoGuesserViewModel.questionBank[currentQuestionId].getAnswer();
        if (answer == AnswerIsCorrect){
            score = score + 1;
            Toast.makeText(this, "CORRECT ANSWER", Toast.LENGTH_SHORT).show();

        }
        else {
            score = score - 0.5;
            Toast.makeText(this, "WRONG ANSWER", Toast.LENGTH_SHORT).show();
        }
    }

    public void shaffleQuestion(){
        List <QuestionModel> questionList= Arrays.asList( GeoGuesserViewModel.questionBank);
        Collections.shuffle(questionList);
        questionList.toArray( GeoGuesserViewModel.questionBank);
    }

    public void refreshScreen(){
        imageView.setImageResource(GeoGuesserViewModel.questionBank[currentQuestionId].getQuestionImage());
        progressText.setText("Question " + (currentQuestionId + 1) + " out of 10");
    }

    public void isFinish(){
        AlertDialog.Builder finalGeoGuesser = new AlertDialog.Builder(this);
        finalGeoGuesser.setTitle("THANKS FOR PLAYING");
        finalGeoGuesser.setMessage("Your final score is " + (score * 10));
        finalGeoGuesser.setNegativeButton("FINISH", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        finalGeoGuesser.setPositiveButton("PLAY AGAIN", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentQuestionId = 0;
                score = 0;
                numberHint = 3;
                hint_button.setVisibility(View.VISIBLE);
                imageView.setImageResource(GeoGuesserViewModel.questionBank[currentQuestionId].getQuestionImage());
                progressText.setText("Question " + (currentQuestionId + 1) + " out of 10");
            }
        });
        AlertDialog alertDialog = finalGeoGuesser.create();
        alertDialog.show();
    }
}