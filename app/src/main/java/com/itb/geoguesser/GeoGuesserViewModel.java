package com.itb.geoguesser;

public class GeoGuesserViewModel extends androidx.lifecycle.ViewModel {

    static final QuestionModel [] questionBank = {
            new QuestionModel(R.drawable.bull, 1),
            new QuestionModel(R.drawable.kangaroo, 4),
            new QuestionModel(R.drawable.elephant, 3),
            new QuestionModel(R.drawable.kiwi, 2),
            new QuestionModel(R.drawable.buffalo, 1),
            new QuestionModel(R.drawable.jaguar,4 ),
            new QuestionModel(R.drawable.deer, 4),
            new QuestionModel(R.drawable.moose, 1),
            new QuestionModel(R.drawable.wolf, 2 ),
            new QuestionModel(R.drawable.dromedary, 3),
    };
}
